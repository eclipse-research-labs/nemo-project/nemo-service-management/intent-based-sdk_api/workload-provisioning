from flask import Flask
from flask_restx import Api

app = Flask(__name__)

api = Api(app, version='0.1.0', title='NEMO - Workload Provisioning',
    description='Create Access Control Services and Routes',
    doc='/swagger'
)