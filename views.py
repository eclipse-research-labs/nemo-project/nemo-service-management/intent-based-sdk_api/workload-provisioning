from flask import Flask, request, jsonify, Response
import os
import json

from flask_restx import Resource

from swagger_models import * 

from utils.utils import create_id, create_service, create_route, create_keycloak_plugin

@submit_workload.route('', methods=['POST'])
class SubmitWorkload(Resource):
    """
    POST a NEMO workload to create a Kong Service and Route
    """

    @submit_workload.doc(responses={ 200: 'OK', 400: 'Bad Request', 500: 'Server Error' },
                         description="POST a NEMO workload",
                         body=submit_workload_body)
    def post(self):
        if request.method == "POST":
            if request.data:
                workload = request.get_json()

                host = workload["host"]
                port = int(workload["port"])
                path = workload["endpoint"]
                service_name = workload["service_name"]
                route_name = workload["route_name"]
                paths = workload["route_paths"]
                client_id = workload["keycloak_client_id"]
                client_secret = workload["keycloak_client_secret"]
                realm = workload["keycloak_realm"]

                service_id = create_id()
                route_id = create_id()

                service_res = create_service(host, path, service_name, port, service_id)
                print(service_res, flush=True)
                route_res = create_route(paths, route_name, route_id, service_name, service_id)
                print(route_res, flush=True)
                keycloak_res = create_keycloak_plugin(route_id, client_id, client_secret, realm)
                print(keycloak_res, flush=True)

                message = {"message": "Kong Service, Route and Keycloak plugin enabled!"}

                return Response(json.dumps(message), mimetype='application/json')  

                


