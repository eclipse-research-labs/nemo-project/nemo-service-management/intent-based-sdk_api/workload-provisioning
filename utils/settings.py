import os

KONG_URL = os.getenv("KONG_URL")
KEYCLOAK_URL = os.getenv("KEYCLOAK_URL")