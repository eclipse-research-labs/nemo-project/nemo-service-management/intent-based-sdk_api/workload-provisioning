import json
import uuid
import time

import requests

import utils.settings as settings

def create_id():
    id = str(uuid.uuid4()) 
    
    return id

def create_timestamp():
    timestamp = int(time.time())

    return timestamp


def create_service(host, path, service_name, port, service_id):
    service_file = open("resources/service.json")
    service = json.load(service_file)
    service_file.close()

    service["host"] = host
    service["path"] = path
    service["name"] = service_name
    service["port"] = port
    service["created_at"] = create_timestamp()
    service["updated_at"] = create_timestamp()
    service["id"] = service_id

    print(service, flush=True)

    kong_base_url = settings.KONG_URL

    url = f"{kong_base_url}/services/"
    headers = {"Content-Type": "application/json"}

    req = requests.post(url=url, data=json.dumps(service), headers=headers)
    print(req, flush=True)
    res = req.text
    
    return res

def create_route(paths, route_name, route_id, service_name, service_id):
    route_file = open("resources/route.json")
    route = json.load(route_file)
    route_file.close()

    route["paths"] = paths
    route["name"] = route_name
    route["created_at"] = create_timestamp()
    route["updated_at"] = create_timestamp()
    route["id"] = route_id
    route["service"]["id"] = service_id

    kong_base_url = settings.KONG_URL

    url = f"{kong_base_url}/services/{service_name}/routes"
    headers = {"Content-Type": "application/json"}

    req = requests.post(url=url, data=json.dumps(route), headers=headers)
    res = req.text
    
    return res


def create_keycloak_plugin(route_id, client_id, client_secret, realm):
    keycloak_file = open("resources/keycloak.json")
    keycloak = json.load(keycloak_file)
    keycloak_file.close()

    keycloak_id = create_id()
    keycloak_url = settings.KEYCLOAK_URL 

    keycloak["created_at"] = create_timestamp()
    keycloak["config"]["keycloakurl"] = keycloak_url
    keycloak["config"]["clientid"] = client_id
    keycloak["config"]["clientsecret"] = client_secret
    keycloak["config"]["realm"] = realm
    keycloak["id"] = keycloak_id
    keycloak["route"]["id"] = route_id

    print(keycloak, flush=True)

    kong_base_url = settings.KONG_URL

    url = f"{kong_base_url}/routes/{route_id}/plugins"
    headers = {"Content-Type": "application/json"}

    req = requests.post(url=url, data=json.dumps(keycloak), headers=headers)
    res = req.text
    
    return res


