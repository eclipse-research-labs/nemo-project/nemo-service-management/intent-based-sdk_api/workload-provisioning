from main import api
from flask_restx import fields


# Endpoint: "/submit_workload"
submit_workload = api.namespace("submit_workload", description="Submit a NEMO workload")
submit_workload_body = api.model("submit_workload", {"host": fields.String(required=True, description="The IP or name of the component"),
                                                     "port": fields.String(required=True, description="The port the component listens to"),
                                                     "endpoint": fields.String(required=True, description="The endpoint of the component that will be protected by the Access Control"),
                                                     "service_name": fields.String(required=True, description="The name of the Kong Service"),
                                                     "route_name": fields.String(required=True, description="The name of the Kong Route"),
                                                     "route_paths": fields.List(fields.String(), required=True, description="The name of the Kong Route endpoints that will be exposed"),
                                                     "keycloak_client_id": fields.String(required=True, description="The Keycloak Client ID for the Keycloal plugin"),
                                                     "keycloak_client_secret": fields.String(required=True, description="The Keycloak Client Secret for the Keycloal plugin"),
                                                     "keycloak_realm": fields.String(required=True, description="The Keycloak Realm for the Keycloal plugin")
                                         })