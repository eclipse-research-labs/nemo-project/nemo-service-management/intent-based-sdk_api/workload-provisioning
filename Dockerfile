FROM python:3.8

WORKDIR /app

COPY Pipfile .
COPY Pipfile.lock .

RUN pip install pipenv

RUN pipenv install

COPY . .

EXPOSE 5000

CMD ["pipenv", "run", "flask", "run", "--host=0.0.0.0"]