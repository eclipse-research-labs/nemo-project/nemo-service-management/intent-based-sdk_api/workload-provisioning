# NEMO Workload Provisioning

This is the NEMO Workload Provisioning handler. It is responsible for adding a NEMO Workload to the [Kong API Gateway](https://docs.konghq.com/gateway/latest/) to route the requests to the available APIs and provide protection to the different endpoints, with the help of Keycloak.

## Prerequisites

Before the installation, please make sure to have:

- Docker and Docker Compose installed
- An instance of the [NEMO Access Control](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-cybersecurity-and-unified_federated-access-control/access-control/nemo-access-control) deployed
- A Keycloak instance deployed and configured

## Installation

Before starting the application, create a `.env` file with the following environmental variables:

| Enironmental Variable | Description | Example Value |
|   ---     |    ---      |      ---      |
| `KONG_URL` | The URL of the Kong Admin API | `http://localhost:8001` |
| `KEYCLOAK_URL` | The URL of the Keycloak instance | `http://localhost` |

To start the handler, run:

```bash
docker compose up --build
```

To view the available endpoints, visit the available Swagger here: `http://localhost:5000/swagger`.

A NEMO workload, in order to be added to the Kong Gateway, needs to be described in the following format:

```json
{
    "host": "127.0.0.1",
    "port": "8000",
    "endpoint": "/register",
    "service_name": "test",
    "route_name": "test",
    "route_paths": ["/register"],
    "keycloak_client_id": "user",
    "keycloak_client_secret": "secret",
    "keycloak_realm": "nemo"
}
```

The role of each parameter is explained in the following table:

| Parameter | Description |
|   ---     |    ---      |
|  `host`  |	The IP of the service that will be protected from Access Control |
|  `port`  |	The port of the service that will be protected from Access Control |
|  `endpoint` |	The endpoint of the service that will be protected from Access Control |
|  `service_name`  |  The name of the Kong Service  |
|  `route_name` |  The name of the Kong Route  |
|  `route_paths`  |	 The endpoints which will be exposed by the Access Control instead of the real service endpoint  |
|  `keycloak_client_id`  |	The Keycloak Client ID for the service (required to enable the Keycloak plugin)  |
|  `keycloak_client_secret` |   The Keycloak Client secret for the service (required to enable the Keycloak plugin)  |
|  `keycloak_realm`	 |  The Keycloak Realm for the service (required to enable the Keycloak plugin)  |


`POST` the JSON above to the `/submit-wokload` endpoint and visit the `Kong Manager GUI` of your `NEMO Access Control` instance, to view the Kong `Service`, `Route` and `Keycloak Plugin` that was created to protect the NEMO workload. The following figures demonstrate the results of the request to the handler.

![](kong-service.png)
![](kong-route.png)
![](kong-keycloak.png)